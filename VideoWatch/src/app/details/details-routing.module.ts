import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGaurdService as AuthGuard} from '../gaurds/auth-gaurd.service';
import { GetDetailsComponent } from './components/get-details/get-details.component';
import { LibraryComponent } from './components/library/library.component';
import { SubscriptionsComponent } from './components/subscriptions/subscriptions.component';
import { DetailsComponent } from './details.component';

const routes: Routes = [{ 
  path: '', 
  component: DetailsComponent,
  children: [{
    path:'',
    component: GetDetailsComponent
  },{
    path:'subscriptions',
    component: SubscriptionsComponent
  },{
    path:'library',
    component: LibraryComponent,
    canActivate: [AuthGuard]
  }] 
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailsRoutingModule { }
