import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsRoutingModule } from './details-routing.module';
import { GetDetailsComponent } from './components/get-details/get-details.component';

// Material Module
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatMenuModule} from '@angular/material/menu';




// Flex layout module -- for responsive application
import { FlexLayoutModule } from '@angular/flex-layout';

// shared components
import { HeaderComponent } from '../shared/components/header/header.component';
import { NavComponent } from '../shared/components/nav/nav.component';
import { FooterComponent } from '../shared/components/footer/footer.component';

import { DetailsComponent } from './details.component';
import { SubscriptionsComponent } from './components/subscriptions/subscriptions.component';
import { LibraryComponent } from './components/library/library.component';

import {VgCoreModule} from '@videogular/ngx-videogular/core';
import {VgControlsModule} from '@videogular/ngx-videogular/controls';
import {VgOverlayPlayModule} from '@videogular/ngx-videogular/overlay-play';
import {VgBufferingModule} from '@videogular/ngx-videogular/buffering';


@NgModule({
  declarations: [
    GetDetailsComponent, 
    HeaderComponent, 
    DetailsComponent, 
    NavComponent,
    FooterComponent,
    SubscriptionsComponent,
    LibraryComponent
  ],
  imports: [
    CommonModule,
    DetailsRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    FlexLayoutModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    MatDividerModule,
    MatMenuModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule
  ]
})
export class DetailsModule { }
