import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { AuthService } from '../../auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  loginForm: any;
  
  constructor(public fb: FormBuilder, public loginService: AuthService, private snackBar: MatSnackBar) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    })
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password')
  }

  

  ngOnInit(): void {
  }

  onSubmit(){

    this.loginService.login(this.loginForm.value).subscribe(
      (response) => {
        console.log(response.token)
        localStorage.setItem('access_token', response.token);      },
      (error) => {
        this.snackBar.open(error.error.non_field_errors, 'close', {
          horizontalPosition: 'right',
          verticalPosition: 'top',
          duration: 3000
        });
      }
    )
  }
}
