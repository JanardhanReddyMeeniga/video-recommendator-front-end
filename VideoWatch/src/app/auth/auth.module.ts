import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'

// routing module for Auth module
import { AuthRoutingModule } from './auth.routing.module';

// Flex layout module -- for responsive application
import { FlexLayoutModule } from '@angular/flex-layout';

// ShareButtonModule
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';

// Material Modules
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';


// Material CDK
import {A11yModule} from '@angular/cdk/a11y';

// Components
import { LoginComponent } from './components/login/login.component';

// Shared Components
import { LoginHeaderComponent } from '../shared/components/login-header/login-header.component';
import { LoginFooterComponent } from '../shared/components/login-footer/login-footer.component';

@NgModule({
  declarations: [LoginComponent, LoginHeaderComponent, LoginFooterComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    ShareButtonsModule,
    ShareIconsModule,
    FormsModule,
    ReactiveFormsModule,
    A11yModule,
    HttpClientModule,
    MatSnackBarModule
  ]
})
export class AuthModule { }
