import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
// import 'rxjs/add/operator/map';
import { map } from "rxjs/operators";
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http:HttpClient, public jwtHelper: JwtHelperService) { }

  public login(reqData:any): Observable<any> {
    return this.http.post(environment.apiUrl + '/auth/login/', reqData)
  }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('access_token')!;
    console.log(token, !this.jwtHelper.isTokenExpired(token))
    
    // Check whether the token is expired and return
    // true or false
    return !this.jwtHelper.isTokenExpired(token);
  }

}
